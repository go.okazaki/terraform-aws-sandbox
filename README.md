Terrafrom
===

## Configuration

1. AWS Client

```shell
export AWS_DEFAULT_REGION=ap-northeast-1
```

* [Profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)

```shell
export AWS_PROFILE=<profile_name>
```

* or [Environment](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html)

```shell
export AWS_ACCESS_KEY_ID=<access_key>
export AWS_SECRET_ACCESS_KEY=<secret_access_key>
```
