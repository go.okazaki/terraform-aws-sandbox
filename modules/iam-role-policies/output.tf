output "iam_role" {
  description = "Resource of IAM role"
  value       = aws_iam_role.this
}

output "iam_policy" {
  description = "Resource of IAM policy"
  value       = aws_iam_policy.this
}
