variable "assume_role_policy" {
  description = "Policy that grants an entity permission to assume the role."
  type        = string
}

variable "description" {
  description = "Description of the role."
  type        = string
  default     = null
}

variable "force_detach_policies" {
  description = "Whether to force detaching any policies the role has before destroying it. Defaults to `false`."
  type        = bool
  default     = null
}

variable "inline_policies" {
  description = "Configuration block defining an exclusive set of IAM inline policies associated with the IAM role."
  type        = map(string)
  default     = {}
}

variable "managed_policy_arns" {
  description = "Set of exclusive IAM managed policy ARNs to attach to the IAM role."
  type        = list(string)
  default     = null
}

variable "max_session_duration" {
  description = "Maximum session duration (in seconds) that you want to set for the specified role."
  type        = number
  default     = null
}

variable "name" {
  description = "Friendly name of the role."
  type        = string
  default     = null
}

variable "name_prefix" {
  description = "Creates a unique friendly name beginning with the specified prefix."
  type        = string
  default     = null
}

variable "path" {
  description = "Path to the role. See [IAM Identifiers](https://docs.aws.amazon.com/IAM/latest/UserGuide/Using_Identifiers.html) for more information."
  type        = string
  default     = null
}

variable "permissions_boundary" {
  description = "ARN of the policy that is used to set the permissions boundary for the role."
  type        = string
  default     = null
}

variable "policies" {
  description = "Key-value mapping of creating IAM policies for the IAM role."
  type = map(object({
    path        = optional(string, "/")
    policy      = string
    description = optional(string)
  }))
  default = {}
}

variable "policy_names" {
  description = "List of attaching IAM policy names for the IAM role."
  type        = list(string)
  default     = []
}

variable "tags" {
  description = "Key-value mapping of tags for the IAM role."
  type        = map(string)
  default     = {}
}

locals {
  policy_names = { for name in var.policy_names : name => name }
}
