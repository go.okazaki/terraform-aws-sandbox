resource "aws_iam_role" "this" {
  assume_role_policy    = var.assume_role_policy
  description           = var.description
  force_detach_policies = var.force_detach_policies
  managed_policy_arns   = var.managed_policy_arns
  max_session_duration  = var.max_session_duration
  name                  = var.name
  name_prefix           = var.name_prefix
  path                  = var.path
  permissions_boundary  = var.permissions_boundary

  dynamic "inline_policy" {
    for_each = var.inline_policies

    content {
      name   = inline_policy.key
      policy = inline_policy.value
    }
  }

  tags = var.tags
}

resource "aws_iam_policy" "this" {
  for_each = var.policies

  name        = each.key
  path        = each.value.path
  description = each.value.description
  policy      = each.value.policy

  tags = var.tags
}

resource "aws_iam_role_policy_attachment" "this" {
  for_each = var.policies

  role       = aws_iam_role.this.name
  policy_arn = aws_iam_policy.this[each.key].arn
}

data "aws_iam_policy" "reference" {
  for_each = local.policy_names

  name = each.key

  tags = var.tags
}

resource "aws_iam_role_policy_attachment" "reference" {
  for_each = local.policy_names

  role       = aws_iam_role.this.name
  policy_arn = data.aws_iam_policy.reference[each.key].arn
}
