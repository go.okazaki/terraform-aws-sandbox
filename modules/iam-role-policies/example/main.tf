provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Environment = var.environment
      Service     = var.service
      Managed     = "Terraform"
    }
  }
}

module "example_iam_role_policy" {
  source = "../"
  name   = "${var.service}-${var.environment}"
  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : {
      "Effect" : "Allow",
      "Principal" : {
        "Service" : "lambda.amazonaws.com"
      },
      "Action" : "sts:AssumeRole"
    }
  })
  inline_policies = {
    "test" = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action   = ["ec2:Describe*"]
          Effect   = "Allow"
          Resource = "*"
        },
      ]
    })
  }
  policies = {
    "${var.service}-${var.environment}" = {
      policy = jsonencode({
        "Version" : "2012-10-17",
        "Statement" : [
          {
            "Effect" : "Allow",
            "Action" : [
              "logs:CreateLogStream",
              "logs:DescribeLogStreams",
              "logs:CreateLogGroup",
              "logs:PutLogEvents"
            ],
            "Resource" : "*"
          }
        ]
      })
    }
  }
  policy_names = ["AmazonS3ReadOnlyAccess"]
}
