resource "aws_security_group" "this" {
  name        = var.name
  description = var.description
  vpc_id      = var.vpc_id

  tags = merge(var.tags, {
    Name = var.name
  })
}

resource "aws_security_group_rule" "ingress" {
  for_each = var.ingress

  from_port                = each.value.from_port
  protocol                 = each.value.protocol
  to_port                  = each.value.to_port
  type                     = "ingress"
  cidr_blocks              = each.value.cidr_blocks
  description              = each.value.description
  ipv6_cidr_blocks         = each.value.ipv6_cidr_blocks
  prefix_list_ids          = each.value.prefix_list_ids
  self                     = each.value.self
  source_security_group_id = each.value.source_security_group_id
  security_group_id        = aws_security_group.this.id
}

resource "aws_security_group_rule" "egress" {
  for_each = var.egress

  from_port                = each.value.from_port
  protocol                 = each.value.protocol
  to_port                  = each.value.to_port
  type                     = "egress"
  cidr_blocks              = each.value.cidr_blocks
  description              = each.value.description
  ipv6_cidr_blocks         = each.value.ipv6_cidr_blocks
  prefix_list_ids          = each.value.prefix_list_ids
  self                     = each.value.self
  source_security_group_id = each.value.source_security_group_id
  security_group_id        = aws_security_group.this.id
}
