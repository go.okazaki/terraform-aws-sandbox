variable "name" {
  description = "Name of the security group."
  type        = string
}

variable "vpc_id" {
  description = "VPC ID."
  type        = string
}

variable "description" {
  description = "Security group description."
  type        = string
  default     = null
}

variable "ingress" {
  description = "Key-value mapping of creating ingress rules for the Security group."
  type = map(object({
    from_port                = number
    protocol                 = string
    to_port                  = number
    cidr_blocks              = optional(list(string))
    description              = optional(string)
    ipv6_cidr_blocks         = optional(list(string))
    prefix_list_ids          = optional(list(string))
    self                     = optional(string)
    source_security_group_id = optional(string)
  }))
  default = {}
}

variable "egress" {
  description = "Key-value mapping of creating egress rules for the Security group."
  type = map(object({
    from_port                = number
    protocol                 = string
    to_port                  = number
    cidr_blocks              = optional(list(string))
    description              = optional(string)
    ipv6_cidr_blocks         = optional(list(string))
    prefix_list_ids          = optional(list(string))
    self                     = optional(string)
    source_security_group_id = optional(string)
  }))
  default = {}
}

variable "tags" {
  description = "Key-value mapping of tags for the Security group."
  type        = map(string)
  default     = {}
}
