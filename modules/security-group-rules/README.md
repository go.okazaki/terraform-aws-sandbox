# security-group-rules

SecurityGroup作成と同時に複数のruleを作成します。

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_security_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.egress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.ingress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_description"></a> [description](#input\_description) | Security group description. | `string` | `null` | no |
| <a name="input_egress"></a> [egress](#input\_egress) | Key-value mapping of creating egress rules for the Security group. | <pre>map(object({<br>    from_port                = number<br>    protocol                 = string<br>    to_port                  = number<br>    cidr_blocks              = optional(list(string))<br>    description              = optional(string)<br>    ipv6_cidr_blocks         = optional(list(string))<br>    prefix_list_ids          = optional(list(string))<br>    self                     = optional(string)<br>    source_security_group_id = optional(string)<br>  }))</pre> | `{}` | no |
| <a name="input_ingress"></a> [ingress](#input\_ingress) | Key-value mapping of creating ingress rules for the Security group. | <pre>map(object({<br>    from_port                = number<br>    protocol                 = string<br>    to_port                  = number<br>    cidr_blocks              = optional(list(string))<br>    description              = optional(string)<br>    ipv6_cidr_blocks         = optional(list(string))<br>    prefix_list_ids          = optional(list(string))<br>    self                     = optional(string)<br>    source_security_group_id = optional(string)<br>  }))</pre> | `{}` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of the security group. | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Key-value mapping of tags for the Security group. | `map(string)` | `{}` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC ID. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_security_group"></a> [security\_group](#output\_security\_group) | Resource of Security group. |
<!-- END_TF_DOCS -->