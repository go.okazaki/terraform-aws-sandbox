provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Environment = var.environment
      Service     = var.service
      Managed     = "Terraform"
    }
  }
}

resource "aws_vpc" "main" {
  cidr_block           = "172.100.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "vpc-${var.service}-${var.environment}"
  }
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id       = aws_vpc.main.id
  service_name = "com.amazonaws.${var.region}.s3"

  tags = {
    Name = "ep-s3-${var.environment}"
  }
}

module "example_lb_security_group" {
  source = "../"
  name   = "scg-${var.service}-lb-${var.environment}"
  vpc_id = aws_vpc.main.id
  egress = {
    "app" = {
      description              = "to app"
      from_port                = 443
      to_port                  = 443
      protocol                 = "tcp"
      source_security_group_id = module.example_app_security_group.security_group.id
    }
  }
}

module "example_app_security_group" {
  source = "../"
  name   = "scg-${var.service}-app-${var.environment}"
  vpc_id = aws_vpc.main.id
  ingress = {
    "lb" = {
      description              = "from lb"
      from_port                = 443
      to_port                  = 443
      protocol                 = "tcp"
      source_security_group_id = module.example_lb_security_group.security_group.id
    }
  }
  egress = {
    "s3_endpoint" = {
      description     = "to s3 endpoint"
      from_port       = 443
      to_port         = 443
      protocol        = "tcp"
      prefix_list_ids = [aws_vpc_endpoint.s3.prefix_list_id]
    }
  }
}
